// INTERACTION ELEMENT CLASSES

class Modal {
    constructor() {
        this.modal = document.createElement('div');
        this.modal_body = document.createElement('div');
        this.modal_content = document.createElement('div');
        
        this.modal_title = document.createElement('p');
        this.modal_close = document.createElement('a');
    }

    remove() {
        this.modal.remove();
    }

    create() {
        this.modal.classList.add('modal', 'modal--active');
        
        this.modal_body.className = 'modal__body';
        this.modal_content.className = 'modal__content';
        this.modal_close.className = 'modal__close';

        this.modal_close.setAttribute('href', '#');
        this.modal_close.textContent = cross_symbol;
        this.modal_close.addEventListener('click', () => {
            if (this.modal.classList.contains('modal--active')) {
                this.modal.classList.remove('modal--active');
                this.remove();
            }
        });

        this.modal_content.append(this.modal_close);
        this.modal_body.append(this.modal_content);
        this.modal.append(this.modal_body);
        
        document.body.append(this.modal);

        return this.modal;
    }

    title(titleText = nil, initClass = nil) {
        this.modal_title.textContent = titleText;
        this.modal_title.classList.add('modal-title', initClass);
        this.modal_content.prepend(this.modal_title);
    }

    open() {
        if (!this.modal.classList.contains('modal--active')) {
            this.modal.classList.add('modal--active');
        }
    }

    close() {
        if (this.modal.classList.contains('modal--active')) {
            this.modal.classList.remove('modal--active');
            this.modal.remove();
        }
    }

    insert(...elements) {
        return this.modal_content.append(...elements);
    }
}

class interact_form {
    constructor() {
        this.form = document.createElement('form');
    }

    remove() {
        this.form.remove();
    }

    create() {
        this.form.classList.add('form');
        
        return this.form;
    }

    insert(...elements) {
        this.form.append(...elements);
    }
}

class interact_text {
    constructor() {
        this.text = document.createElement('textarea');
    }

    remove() {
        this.text.remove();
    }

    create() {
        return this.text;
    }

    initAttribs(id = nil, value = nil, placeholder = nil, required = nil) {
        this.text.setAttribute('id', id);
        this.text.setAttribute('value', value);
        this.text.setAttribute('placeholder', placeholder);
        this.text.setAttribute('required', required);
        this.text.setAttribute('maxlength', text_area_maxlength);
    }

    get value() {
        return this.text.value;
    }

    set value(newValue) {
        this.text.value = newValue;
    }

    label(text = nil, initClass = nil) {
        const label = document.createElement('label');
        label.textContent = text;
        label.classList.add(initClass);
        label.setAttribute('for', this.text.id);

        this.text.parentElement.insertBefore(label, this.text);
    }
}

class interact_input {
    constructor() {
        this.input = document.createElement('input');
    }

    remove() {
        this.input.remove();
    }

    create() {
        return this.input;
    }

    initAttribs(id = nil, type = 'text', name = nil, value = nil, placeholder = nil, required = nil) {
        this.input.setAttribute('id', id);
        this.input.setAttribute('type', type);
        this.input.setAttribute('name', name);
        this.input.setAttribute('value', value);
        this.input.setAttribute('placeholder', placeholder);
        this.input.setAttribute('required', required);
        this.input.setAttribute('maxlength', text_area_maxlength_input);
    }

    get value() {
        return this.input.value;
    }

    set value(newValue) {
        this.input.value = newValue;
    }

    error() {
        const input_nextelement = this.input.nextElementSibling;

        if (input_nextelement.classList.contains('errorWarning')) {
            input_nextelement.remove();
        }

        const errorWarning = document.createElement("p");

        errorWarning.textContent = 'Неверно заполнено поле!';
        errorWarning.classList.add('errorWarning');

        if (!input_is_correct(this.input)) {
            if (input_nextelement.classList.contains('errorWarning')) {
                this.input.classList.remove('errorInput');
                errorWarning.remove();
            }
    
            this.input.after(errorWarning);
            this.input.classList.add('errorInput');
        }
    
        this.input.addEventListener('blur', () => {
            if (input_is_correct(this.input)) {
                this.input.classList.remove('errorInput');
                errorWarning.remove();
            }
        });
    }

    event(evt = nil, fn = {}) {
        this.input.addEventListener(evt, fn);
    }

    label(text = nil, initClass = nil) {
        const label = document.createElement('label');
        label.textContent = text;
        label.classList.add(initClass) 
        label.setAttribute('for', this.input.id);

        this.input.parentElement.insertBefore(label, this.input);
    }
}

class interact_select {
    constructor() {
        this.select = document.createElement('select');
    }

    remove() {
        this.select.remove();
    }

    create() {
        return this.select;
    }

    initAttribs(id = nil, disabled) {
        this.select.setAttribute('id', id);

        if (disabled) {
            this.select.setAttribute('disabled', 'disabled');
        }
    }

    get value() {
        return this.select.value;
    }

    set value(newValue) {
        this.select.value = newValue;
    }

    addOption(text = nil, value = nil) {
        const option = document.createElement('option');
        option.textContent = text;
        option.setAttribute('value', value);

        this.select.append(option);

        return option;
    }

    label(text = nil) {
        const label = document.createElement('label');
        label.textContent = text;
        label.setAttribute('for', this.select.id);

        this.select.parentElement.insertBefore(label, this.select);
    }
}
